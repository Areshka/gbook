<?php
session_start();
try {
    require_once __DIR__ . '/config.php';
    require_once __DIR__ . '/function.lib.php';

    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        $subject = trim($_POST['subject']);
        $comment = trim($_POST['comment']);
        $flag = true;

        if (empty($comment)) {
            $error_comment = " Write in field";
            $flag = false;
        }

        if ($flag == true) {
            if ($_SESSION['name']) {
                $array_comment = array(
                    'user_id' => $_SESSION['user_id'],
                    'subject' => $subject,
                    'comment' => $comment,
                    'dateadd' => date("Y-m-d H:i:s"));
                createComment($array_comment);
            }else{
                $error_create_comment = "Please registration or logining";
            }
        }
    }

    if ($_SESSION['name']) {
        echo "You are " . $_SESSION['name'];
    }

    if ($_SESSION['error']) {
        echo $_SESSION['error'];
    }

    if ($_SESSION['name_error']) {
        echo $_SESSION['name_error'];
    }

    $arr = allComments();
    $db = null;
}
catch (PDOException $e) {
    die("Error: " . $e->getMessage());
}

?>
<form action="login/login.php" method="post">
    <span <?= $_SESSION['name'] ? "hidden" : ""; ?>>Login: <input type="text" name="name" title="name"  ></span>
    <span  <?= $_SESSION['name'] ? "hidden" : ""; ?>>Email: <input type="email" name="email" title="email"></span>
    <a href="login/login.php"><input type="submit" value="Login" <?= $_SESSION['name'] ? "hidden" : ""; ?>></a>
</form>
<a href="users/add_contact.php"><input type="submit" value="Registration"></a>
<a href="login/close.php"><input type="submit" value="Exit" <?= !$_SESSION['name'] ? "hidden" : ""; ?>></a>

<br><br>

<form action="<?= $_SERVER['PHP_SELF']; ?>" method="POST">

    <table>
        <tr>
            <td>Subject: </td>
            <td><input type="text" title="subject" name="subject"></td>
        </tr>
        <tr>
            <td>Comment: </td>
            <td>
                <textarea placeholder="Enter comment" name="comment" cols="50" rows="5"></textarea>
                <?php if ($error_create_comment) : ?>
                    <?= $error_create_comment; ?>
                <?php endif; ?>
            </td>

        </tr>
    </table>
    <input type="submit" value="Send"><br><br>
</form>


<?php foreach ($arr as $key => $value) : ?>
        From:  <?= $value['name']; ?> , <?= $value['dateadd']; ?><br>
        Subject: <?= $value['subject']; ?>
    <hr>
        <?= $value['comment']; ?> <br><br>
<?php endforeach; ?>
