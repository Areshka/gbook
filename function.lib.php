<?php

//add new contact
function createUser ($array_user) {
    global $db;
    $rows = $db->prepare("INSERT INTO users (name, email) VALUES (:name, :email)");
    $rows->execute($array_user);
    return $rows;
}

//select all user
function allUsers() {
    global $db;
    $arr = array();
    $result = $db->query("SELECT u.* FROM users AS u");
    while ($res = $result->fetch(PDO::FETCH_BOTH)) {
        $arr[] = $res;
    }
    return $arr;
}

//select one user
function oneUsers($id) {
    global $db;
    $arr = array();
    $result = $db->query("SELECT u.* FROM users AS u WHERE id=" . $id);
    while ($res = $result->fetch(PDO::FETCH_BOTH)) {
        $arr[] = $res;
    }
    return $arr;
}

//select user for registration check
function oneUsersssssssssssssssss($name) {
    global $db;
    $arr = array();
    $result = $db->query("SELECT u.* FROM users AS u WHERE name=" . $name);
    while ($res = $result->fetch(PDO::FETCH_BOTH)) {
        $arr[] = $res;
    }
    return $arr;
}

//delete User
function deleteUser ($id) {
    global $db;
    $db->exec("DELETE FROM users WHERE id=" . $id);
}

//edit User
function editUser ($array_user, $id) {
    global $db;
    $rows = $db->prepare("UPDATE users SET name=:name, email=:email WHERE id=" . $id);
    $rows->execute($array_user);
}


//create comment
function createComment ($array_comments){
    global $db;
    $rows = $db->prepare("INSERT INTO comments (user_id, subject, comment, dateadd) VALUES (:user_id, :subject, :comment, :dateadd)");
    $rows->execute($array_comments);
    return $rows;
}

//select comment
function allComments () {
    global $db;
    $arr = array();
    $result = $db->query("SELECT c.*, u.name FROM comments AS c INNER JOIN users AS u ON c.user_id = u.id ORDER BY dateadd DESC");
    while ($res = $result->fetch(PDO::FETCH_BOTH)) {
        $arr[] = $res;
    }
    return $arr;
}