<?php
session_start();

try {
    require_once __DIR__ . '/../config.php';
    require_once __DIR__ . '/../function.lib.php';

    if ($_GET['id']) {
        $arr = oneUsers($_GET['id']);
        foreach ($arr as $key => $value) {
            $_SESSION['id'] = $value['id'];
            $name = $value['name'];
            $email = $value['email'];
        }
    }

    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        $_SESSION['name'] = trim($_POST['name']);
        $email = trim($_POST['email']);
        $flag = true;
        $count_str = strlen($_SESSION['name']);

        if ($count_str > 20) {
            var_dump($_SESSION['name']);
            $error_name = " Your name must be less than 20";
            $flag = false;
        }
        if (empty($_SESSION['name'])) {
            $error_name = " Write in field";
            $flag = false;
        }

        if (strlen($email) > 50) {
            $error_email = " Your name must be less than 50 characters";
            $flag = false;
        }
        if (empty($email)) {
            $error_email = " Write in field";
            $flag = false;
        }

        if ($flag == true) {
            $arr = allUsers();
            foreach ($arr as $key => $value) {
                if ($_SESSION['name'] == $value['name'] && $email == $value['email']){
                    $error_name_exist = " User already exist!!!";
                }
            }
            if (!$error_name_exist){
                $array_user = array('name' => $_SESSION['name'],
                    'email' => $email);
                editUser($array_user, $_SESSION['id']);
                header("Location: add_contact.php");
            }
        }
    }
    $db = null;
}
catch (PDOException $e) {
    die("Error: " . $e->getMessage());
}

?>

<form action="<?= $_SERVER['PHP_SELF']; ?>" method="POST">
    <table>
        <tr>
            <td>User name: </td>
            <td><input type="text" name="name" title="name" value="<?= $name ?>">
                <?php if($error_name) : ?>
                    <?= $error_name; ?>
                <?php endif; ?>

                <?php if($error_name_exist) : ?>
                    <?= $error_name_exist; ?>
                <?php endif; ?>
            </td>
        </tr>
        <tr>
            <td>Email: </td>
            <td><input type="email" name="email" title="email" value="<?= $email; ?>">
                <?php if($error_email) : ?>
                    <?= $error_email; ?>
                <?php endif; ?>
            </td>
        </tr>
    </table>
    <input type="submit" value="Edit"><br><br>
</form>

