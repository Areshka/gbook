<?php
try {
    require_once __DIR__ . "/../function.lib.php";
    require_once __DIR__ . "/../config.php";

    if ($_GET['id']) {
        deleteUser($_GET['id']);
        header("Location: " . $_SERVER['HTTP_REFERER']);
    }
    $db = null;
}
catch(PDOException $e) {
    die("Error: " . $e->getMessage());
}