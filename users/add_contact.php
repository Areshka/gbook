<?php
session_start();
try {
    require_once __DIR__ . '/../config.php';
    require_once __DIR__ . '/../function.lib.php';

    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        $_SESSION['name'] = trim($_POST['name']);
        $email = trim($_POST['email']);
        $flag = true;
        $count_str = strlen($_SESSION['name']);

        if ($count_str > 20) {
            var_dump($_SESSION['name']);
            $error_name = " Your name must be less than 20";
            $flag = false;
        }
        if (empty($_SESSION['name'])) {
            $error_name = " Write in field";
            $flag = false;
        }

        if (strlen($email) > 50) {
            $error_email = " Your email must be less than 50 characters";
            $flag = false;
        }
        if (empty($email)) {
            $error_email = " Write in field";
            $flag = false;
        }

        if ($flag == true) {
            $arr = allUsers();
            foreach ($arr as $key => $value) {
                if ($_SESSION['name'] == $value['name'] && $email == $value['email']){
                    $error_name_exist = " User already exist!!!";
                }
            }
            if (!$error_name_exist) {
                $array_user = array(
                    'name' => $_SESSION['name'],
                    'email' => $email
                );
                createUser($array_user);
                $_SESSION['user_id'] = $db->lastInsertId();
                header("Location: " . $_SERVER['HTTP_REFERER']);
                exit;
            }
        }
    }
    $arr = allUsers();
    $db = null;
}
catch (PDOException $e) {
    die("Error: " . $e->getMessage());
}
?>


<form action="<?= $_SERVER['PHP_SELF']; ?>" method="POST">
    <table>
        <tr>
            <td>User name: </td>
            <td><input type="text" name="name" title="name">
                <?php if($error_name) : ?>
                    <?= $error_name; ?>
                <?php endif; ?>

                <?php if($error_name_exist) : ?>
                    <?= $error_name_exist ?>
                <?php endif; ?>
            </td>
        </tr>
        <tr>
            <td>Email: </td>
            <td><input type="email" name="email" title="email">
                <?php if($error_email) : ?>
                    <?= $error_email; ?>
                <?php endif; ?>
            </td>
        </tr>
    </table>
    <input type="submit" value="Add"><br><br>
</form>

<table border="1">
    <?php foreach ($arr as $key => $value) : ?>
        <tr>
            <td><?= $value['id']; ?></td>
            <td><?= $value['name']; ?></td>
            <td><?= $value['email']; ?></td>
            <td><a href="delete_contact.php?id=<?= $value['id']; ?>">DELETE</a></td>
            <td><a href="edit_contact.php?id=<?= $value['id']; ?>">EDIT</a></td>
        </tr>
    <?php endforeach; ?>
</table>
