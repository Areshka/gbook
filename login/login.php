<?php
session_start();

try {
    //$config =
        require_once __DIR__ . "/../config.php";
    require_once __DIR__ . "/../function.lib.php";

    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        $name = trim($_POST['name']);
        $email = trim($_POST['email']);
        if (empty($name) && empty($email)) {
            unset($_SESSION['name']);
            $_SESSION['error'] = "Enter login or email";
        } else {
            $users = allUsers();
            foreach ($users as $key => $value) {
                if ($name == $value['name'] && $email == $value['email']) {
                    unset($_SESSION['error']);
                    unset($_SESSION['name_error']);
                    $_SESSION['name'] = $name;
                    $_SESSION['email'] = $email;
                    $_SESSION['user_id'] = $value['id'];
                }
            }

            if (!$_SESSION['name']) {
                unset($_SESSION['error']);
                unset($_SESSION['name']);
                $_SESSION['name_error'] = "User wich such data does not exist";
            }
        }
        header("Location: /../index.php");
    }
    $db = null;
} catch (PDOException $e) {
    die("Error: " . $e->getMessage());
}

